const express = require("express");
const router = express.Router();
const auth = require("../auth")

const userController = require("../controllers/userControllers")
	// Route for checking Email
	router.post("/checkEmail", userController.checkEmailExists);
	// route for registration
	router.post("/register", userController.registerUser);
	// route for log in
	router.post("/login", userController.loginUser);

	// route for retrieving user details
	router.get("/details", userController.userDetails);

module.exports = router;